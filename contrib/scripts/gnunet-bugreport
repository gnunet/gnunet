#!/bin/sh

progname=${0##*/}

statusmsg()
{
    echo "    $@"
}

infomsg()
{
    statusmsg "INFO:    $@"
}

warningmsg()
{
    statusmsg "WARNING: $@"
}

errormsg()
{
    statusmsg "ERROR:   $@"
}

linemsg()
{
    statusmsg "========================================="
}

# It is okay to assume which(1) here because we provide
# more than 1 fallback.
TEST=`type type|grep not`
if test -n "$TEST"; then
  WHICH=which
else
  WHICH=type
fi

os_check()
{
    OS=`uname -s 2>/dev/null`
    infomsg "OS             : $OS"
    REL=`uname -r 2>/dev/null`
    infomsg "OS RELEASE     : $REL"
    HW=`uname -m 2>/dev/null`
    infomsg "HARDWARE       : $HW"
}

# We shouldn't use awk to test for awk... but if
# awk isn't there it can't be found.
awk_check()
{
    if test -n "`awk 2>&1 | tail -1 | awk '{print $1}'`"; then
        infomsg    "awk            : Found"
    else
        warningmsg "awk            : Not found!"
    fi
}

gcc_check()
{
    TEST=`$WHICH gcc | grep -v "not found" 2>/dev/null`
    if test -n "$TEST"; then
        VERS=`gcc --version 2>/dev/null | head -n 1`
        infomsg "gcc            : $VERS"
        # The elif will work in bourne shells, no other shells tested.
    elif test -n "`gcc 2>&1 | tail -1 | awk '{print $1}'`"; then
        VERS=`gcc --version 2>/dev/null | head -n 1`
        infomsg "gcc             : $VERS"
    else
        warningmsg "gcc            : Not Found";
    fi
}

cc_check()
{
    TEST=`$WHICH cc | grep -v "not found" 2>/dev/null`
    if test -n "$TEST"; then
        VERS=`cc --version 2>/dev/null | head -n 1`
        infomsg "cc             : $VERS"
    else
        warningmsg "cc             : Not Found";
    fi
}

cplusplus_check()
{
    TEST=`$WHICH c++ | grep -v "not found" 2>/dev/null`
    if test -n "$TEST"; then
        VERS=`c++ --version 2>/dev/null | head -n 1`
        infomsg "c++            : $VERS"
    else
        warningmsg "c++            : Not Found";
    fi
}

gmake_check()
{
    TEST=`$WHICH gmake | grep -v "not found"  2>/dev/null`
    if test -n "$TEST" ; then
        VER=`gmake --version 2>/dev/null |\
	    awk -F, '{print $1}' |\
	    awk '/GNU Make/{,$NF}'`
        infomsg "gmake            : $VER"
    else
        TEST=`make --version 2>/dev/null`
        if test -n "$TEST"; then
	    VER=`make --version 2>/dev/null |\
	        awk -F, '{print $1}' |\
	        awk '/GNU Make/{print $NF}'`
            infomsg "gmake          : $VER"
        else
	    warningmsg "gmake           : Not Found"
        fi
    fi
}

# Applies for NetBSD make and possibly every make.
make_check()
{
    if test -n "`make 2>/dev/null`"; then
        infomsg    "make           : Found"
    else
        warningmsg "make           : Not Found"
    fi
}

autoconf_check()
{
    TEST=`$WHICH autoconf | grep -v "not found" 2>/dev/null`
    if test -n "$TEST"; then
        autoconf --version |\
            head -n 1 |\
            awk '{\
	if (length($4) == 0) {\
		print "    INFO:    autoconf       : "$3\
	} else {\
		print "    INFO:    autoconf       : "$4\
	}}'
    else
        warningmsg "autoconf       : Not Found"
    fi
}

automake_check()
{
    TEST=`$WHICH automake | grep -v "not found" 2>/dev/null`
    if test -n "$TEST"; then
        VER=`automake --version 2>/dev/null | head -n 1 | awk '{print $4}'`
        infomsg "automake       : $VER"
    else
        warningmsg "automake       : Not Found"
    fi
}

# TODO: More libtool variants.
libtool_check()
{
    TEST=`$WHICH libtoolize | grep -v "not found" 2>/dev/null`
    if test -n "$TEST"; then
        VER=`libtoolize --version 2>/dev/null | head -n 1 | awk '{print $4}'`
        infomsg "libtool        : $VER"
    else
        warningmsg "libtool        : Not Found"
    fi
}

libextractor_check()
{
    TEST=`$WHICH extract | grep -v "not found" 2>/dev/null`
    if test -n "$TEST"; then
        VER=`extract -v 2>/dev/null | head -n 1 | awk '{print $2}'`
        infomsg "libextractor   : $VER"
    else
        warningmsg "libextractor   : Not Found"
    fi
}

gnunet08_check()
{
    if test -x gnunetd; then
        VER=`gnunetd -v | sed -e "s/v//" 2>/dev/null`
        warningmsg "GNUnet 0.8     : $VER (may conflict!)"
    else
        infomsg "GNUnet 0.8     : Not Found (good)"
    fi
}

gnunet09x_check()
{
    TEST=`$WHICH gnunet-arm | grep -v "not found" 2>/dev/null`
    if test -n "$TEST"; then
        VER=`gnunet-arm -v | sed -e "s/v//" 2>/dev/null | awk '{print $2}'`
        VER9=`echo $VER | grep ^0\.9\.`
        if test -n "$VER9"; then
            warningmsg "GNUnet 0.9     : $VER"
        else
            infomsg "GNUnet 0.9     : Not Found (good)"
        fi
    else
        infomsg "GNUnet 0.9     : Not Found (good)"
    fi
}

gnunet010x_check()
{
    TEST=`$WHICH gnunet-arm | grep -v "not found" 2>/dev/null`
    if test -n "$TEST"; then
        VER=`gnunet-arm -v | sed -e "s/v//" 2>/dev/null | awk '{print $2}'`
        VER9=`echo $VER | grep ^0\.10\.`
        if test -n "$VER10"; then
            warningmsg "GNUnet 0.10    : $VER"
        else
            infomsg "GNUnet 0.10    : Not Found (good)"
        fi
    else
        infomsg "GNUnet 0.10    : Not Found (good)"
    fi
}

gnunet011x_check()
{
    TEST=`$WHICH gnunet-arm | grep -v "not found" 2>/dev/null`
    if test -n "$TEST"; then
        VER=`gnunet-arm -v | sed -e "s/v//" 2>/dev/null | awk '{print $2}'`
        infomsg "GNUnet 0.11    : $VER"
    else
        warningmsg "GNUnet 0.11.x     : Not Found"
    fi
}

gcrypt_check()
{
    TEST=`$WHICH libgcrypt-config | grep -v "not found" 2> /dev/null`
    if test -n "$TEST"; then
        VER=`libgcrypt-config --version 2> /dev/null`
        infomsg "libgcrypt      : $VER"
    else
        warningmsg "libgcrypt      : Not Found"
    fi
}

mysql_check()
{
    TEST=`$WHICH mysql_config | grep -v "not found" 2> /dev/null`
    if test -n "$TEST"; then
        VER=`mysql_config --version 2> /dev/null`
        infomsg "mysql          : $VER"
    else
        infomsg "mysql          : Not Found"
    fi
}

pkgconf_check()
{
    TEST=`$WHICH pkgconf | grep -v "not found" 2> /dev/null`
    if test -n "$TEST"; then
        pkgconf --version 2> /dev/null | awk '{print "pkgconf     : "$1}'
    else
        infomsg "pkgconf        : Not Found"
    fi
}

pkgconfig_check()
{
    TEST=`$WHICH pkg-config | grep -v "not found" 2> /dev/null`
    if test -n "$TEST"; then
        VER=`pkg-config --version 2> /dev/null | awk '{print $1}'`
        infomsg "pkg-config     : $VER"
    else
        infomsg "pkg-config     : Not Found"
    fi
}

glib2_check()
{
    TEST=`$WHICH pkg-config | grep -v "not found" 2> /dev/null`
    if test -n "$TEST"; then
        VER=`pkg-config --modversion glib-2.0 2> /dev/null | awk '{print $1}'`
        infomsg "glib2          : $VER"
    else
        infomsg "glib2          : Not Found"
    fi
}

gtk2_check()
{
    TEST=`$WHICH pkg-config | grep -v "not found" 2> /dev/null`
    if test -n "$TEST"; then
        VER=`pkg-config --modversion gtk+-2.0 2> /dev/null | awk '{print $1}'`
        if test -n "$VER"; then
            infomsg "gtk2+          : $VER"
        else
            infomsg "gtk2+          : Not found"
        fi
    else
        infomsg "gtk2+          : Not Found"
    fi
}

gtk3_check()
{
    TEST=`$WHICH pkg-config | grep -v "not found" 2> /dev/null`
    if test -n "$TEST"; then
        VER=`pkg-config --modversion gtk+-3.0 2> /dev/null | awk '{print $1}'`
        if test -n "$VER"; then
            infomsg "gtk3+          : $VER"
        else
            infomsg "gtk3+          : Not found"
        fi
    else
        infomsg "gtk3+          : Not Found"
    fi
}

gtk4_check()
{
    TEST=`$WHICH pkg-config | grep -v "not found" 2> /dev/null`
    if test -n "$TEST"; then
        VER=`pkg-config --modversion gtk+-4.0 2> /dev/null | awk '{print $1}'`
        if test -n "$VER"; then
            infomsg "gtk4+          : $VER"
        else
            infomsg "gtk4+          : Not found"
        fi
    else
        infomsg "gtk4+          : Not Found"
    fi
}

gmp_check()
{
    TEST=`$WHICH dpkg | grep -v "not found" 2> /dev/null`
    if test -n "$TEST"; then
        LINES=`dpkg -s libgmp-dev | grep Version | wc -l 2> /dev/null`
        if test "$LINES" = "1"
        then
            VERSION=`dpkg -s libgmp-dev | grep Version | awk '{print $2}'`
            infomsg "GMP            : libgmp-dev-$VERSION.deb"
        else
            errormsg "GMP            : dpkg: libgmp-dev not installed"
        fi
    else
        TEST=`$WHICH rpm | grep -v "not found" 2> /dev/null`
        if test -n "$TEST"; then
            rpm -q gmp | sed -e "s/gmp-//" 2> /dev/null | \
                infomsg "GMP            : $1.rpm"
        else
            warningmsg "GMP            : Test not available"
        fi
    fi
}

libunistring_check()
{
    TEST=`$WHICH dpkg | grep -v "not found" 2> /dev/null`
    if test -n "$TEST"; then
        LINES=`dpkg -s libunistring-dev | grep Version | wc -l`
        if test "$LINES" = "1"
        then
            VERSION=`dpkg -s libunistring-dev | grep Version | awk '{print $2}'`
            infomsg "libunistring   : libunistring3-dev-$VERSION.deb"
        else
            errormsg "libunistring   : dpkg: libunistring3-dev not installed"
        fi
    else
        TEST=`$WHICH rpm | grep -v "not found" 2> /dev/null`
        if test -n "$TEST"; then
            rpm -q unistring | sed -e "s/unistring-//" 2> /dev/null | \
                awk '{print "libunistring   : "$1.rpm}'
        else
            infomsg "libunistring   : Test not available"
        fi
    fi
}

pkgadd_check()
{
    TEST=`$WHICH pkg_add | grep -v "not found" 2> /dev/null`
    if test -n "$TEST"; then
        VERSION_UNISTRING=`pkg_info -Nb libunistring`
        VERSION_GMP=`pkg_info -Nb gmp`
        echo "libunistring     :"
        echo "$VERSION_UNISTRING"
        echo "GMP              :"
        echo "$VERSION_GMP"
    fi
}

gettext_check()
{
    TEST=`$WHICH gettext | grep -v "not found" 2> /dev/null`
    if test -n "$TEST"; then
        VER=`gettext --version | head -n1 2> /dev/null |  awk '{print $4}'`
        infomsg "GNU gettext    : $VER"
    else
        errormsg "GNU gettext    : Not found"
    fi
}

# Merge curl_check + gnurl_check -> error if neither is
# found (yes those systems exist)
curl_check()
{
    TEST=`$WHICH curl-config | grep -v "not found" 2> /dev/null`
    if test -n "$TEST"; then
        VER=`curl-config --version | head -n1 2> /dev/null | awk '{print $2}'`
        infomsg "libcurl        : $VER"
    else
        infomsg "libcurl        : Not found"
    fi
}

gnurl_check()
{
    TEST=`$WHICH gnurl-config | grep -v "not found" 2> /dev/null`
    if test -n "$TEST"; then
        VER=`gnurl-config --version | head -n1 2> /dev/null | awk '{print "$2}'`
        infomsg "libgnurl       : $VER"
    else
        infomsg "libgnurl       : Not found"
    fi
}

libmicrohttpd_check()
{
    TMPFILE=`mktemp /tmp/mhd-version-testXXXXXX`
    cat - >$TMPFILE.c <<EOF
#include <microhttpd.h>
#include <stdio.h>
int main()
{
  fprintf (stdout, "%X\n", MHD_VERSION);
  return 0;
}
EOF
    VER="Not found"
    gcc -o $TMPFILE $TMPFILE.c 2> /dev/null && VER=`$TMPFILE`
    cc -o $TMPFILE $TMPFILE.c 2> /dev/null && VER=`$TMPFILE`
    rm -f $TMPFILE $TMPFILE.bin
    infomsg "libmicrohttpd  : $VER"
}

glpk_check()
{
    TMPFILE=`mktemp /tmp/glpk-version-testXXXXXX`
    cat - >$TMPFILE.c <<EOF
#include <glpk.h>
#include <stdio.h>
int main()
{
  fprintf (stdout, "%u.%u\n", GLP_MAJOR_VERSION, GLP_MINOR_VERSION);
  return 0;
}
EOF
    VER="Not found"
    gcc -o $TMPFILE $TMPFILE.c 2> /dev/null && VER=`$TMPFILE`
    cc -o $TMPFILE $TMPFILE.c 2> /dev/null && VER=`$TMPFILE`
    rm -f $TMPFILE $TMPFILE.bin
    infomsg "GNU GLPK       : $VER"
}

gnutls_check()
{
    TMPFILE=`mktemp /tmp/gnutls-version-testXXXXXX`
    cat - >$TMPFILE.c <<EOF
#include <gnutls/gnutls.h>
#include <stdio.h>
int main()
{
  fprintf (stdout, "%s\n", GNUTLS_VERSION);
  return 0;
}
EOF
    VER="Not found"
    gcc -o $TMPFILE $TMPFILE.c 2> /dev/null && VER=`$TMPFILE`
    cc -o $TMPFILE $TMPFILE.c 2> /dev/null && VER=`$TMPFILE`
    rm -f $TMPFILE $TMPFILE.bin
    infomsg "GnuTLS         : $VER"
}

main()
{
    infomsg "${progname} 0.11.0"
    infomsg
    infomsg "Please submit the following"
    infomsg "information with your bug report:"
    linemsg
    os_check
    awk_check
    gcc_check
    cc_check
    cplusplus_check
    gmake_check
    make_check
    autoconf_check
    automake_check
    libtool_check
    libextractor_check
    gnunet08_check
    gnunet09x_check
    gnunet010x_check
    gnunet011x_check
    gcrypt_check
    mysql_check
    pkgconf_check
    pkgconfig_check
    glib2_check
    gtk2_check
    gtk3_check
    gtk4_check
    gmp_check
    libunistring_check
    pkgadd_check
    gettext_check
    curl_check
    gnurl_check
    libmicrohttpd_check
    glpk_check
    gnutls_check
    linemsg
    infomsg "Bug report saved in ./my_gnunet_bugreport.log"
}

main "$@" 2>&1 | tee "my_gnunet_bugreport.log"
